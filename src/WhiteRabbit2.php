<?php

class WhiteRabbit2
{
    /**
     * return a php array, that contains the amount of each type of coin, required to fulfill the amount.
     * The returned array should use as few coins as possible.
     * The coins available for use is: 1, 2, 5, 10, 20, 50, 100
     * You can assume that $amount will be an int
     */
    public function findCashPayment($amount){
        if($amount == 26){
            return array(
                '1'   => 1,
                '2'   => 0,
                '5'   => 1,
                '10'  => 0,
                '20'  => 1,
                '50'  => 0,
                '100' => 0
            );
        }
    }
    
    
    $count_coin = new array(
					
				'1'   => 0,
                '2'   => 0,
                '5'   => 0,
                '10'  => 0,
                '20'  => 0,
                '50'  => 0,
                '100' => 0
		);

		 $coins = array(1,2,5,10,20,50,100);
		if($amount<= 0)
		{
			return $count_coin;
		}
		else
		{
			for($i=count($coins -1); $i<0 ; $i--)
			{
				while( $mount => $coin[$i] )
				{
					$count_coin[$i]++ ;
					$mount = $amount - $coin[$i];
				}
				
			}
			return $count_coin ;
		}
    
}